<SMTK_AttributeResource Version="4">
  <Categories>
    <Cat>Enclosure Radiation</Cat>
    <Cat>Genre</Cat>
  </Categories>
  <Definitions>
    <AttDef Type="enclosure" Label="Enclosure" Unique="true" Version="0">
      <AssociationsDef NumberOfRequiredValues="1" Extensible="true">
        <Accepts>
          <Resource Name="smtk::model::Resource" Filter="face"/>
        </Accepts>
      </AssociationsDef>
      <ItemDefinitions>
        <!-- Genre-specific item defs-->
        <Group Name="symmetries" Label="Symmetries" Version="0">
          <Categories>
            <Cat>Genre</Cat>
          </Categories>
          <ItemDefinitions>
            <Void Name="mirror-x" Label="Mirror X" Optional="true" IsEnabledByDefault="false"/>
            <Void Name="mirror-y" Label="Mirror Y" Optional="true" IsEnabledByDefault="false"/>
            <Void Name="mirror-z" Label="Mirror Z" Optional="true" IsEnabledByDefault="false"/>
            <Group Name="rotation" Label="Rotational Symmetry" Optional="true" IsEnabledByDefault="false">
              <ItemDefinitions>
                <String Name="symmetry-axis" Label="Axis">
                  <DiscreteInfo>
                    <Value>X</Value>
                    <Value>Y</Value>
                    <Value>Z</Value>
                  </DiscreteInfo>
                </String>
                <Int Name="order" Label="Order">
                  <BriefDescription>Rotational symmetry of order N speciifies that the geometry is invariant with
respect to rotation by an angle of 360 degrees divided by N.</BriefDescription>
                  <RangeInfo>
                    <Min exclusive="true">1</Min>
                  </RangeInfo>
                </Int>
              </ItemDefinitions>
            </Group>
          </ItemDefinitions>
        </Group>
        <Void Name="blocking_enclosure" Label="Blocking Enclosure?" Optional="true" IsEnabledByDefault="false">
          <Categories>
            <Cat>Genre</Cat>
          </Categories>
        </Void>
        <Group Name="partial_enclosure" Label="Partial Enclosure?" Optional="true" IsEnabledByDefault="false">
          <Categories>
            <Cat>Genre</Cat>
          </Categories>
          <ItemDefinitions>
            <Double Name="partial_area" Label="Partial Area">
              <Range>
                <Min inclusive="true">0.0</Min>
              </Range>
            </Double>
          </ItemDefinitions>
        </Group>
        <Int Name="hemicube_resolution" Label="Hemicube Resolution">
          <Categories>
            <Cat>Genre</Cat>
          </Categories>
          <DefaultValue>300</DefaultValue>
          <Range>
            <Min inclusive="false">0</Min>
          </Range>
        </Int>
        <Double Name="min_separation" Label="Minimum Separation">
          <Categories>
            <Cat>Genre</Cat>
          </Categories>
          <DefaultValue>10.0</DefaultValue>
          <Range>
            <Min inclusive="false">0.0</Min>
          </Range>
        </Double>
        <Int Name="max_subdivisions" Label="Maximum Subdivisions">
          <Categories>
            <Cat>Genre</Cat>
          </Categories>
          <DefaultValue>50</DefaultValue>
          <Range>
            <Min inclusive="false">0</Min>
          </Range>
        </Int>
        <Int Name="verbosity_level" Label="Verbosity Level" AdvanceLevel="1">
          <Categories>
            <Cat>Genre</Cat>
          </Categories>
          <DefaultValue>2</DefaultValue>
          <Range>
            <Min inclusive="true">0</Min>
          </Range>
        </Int>
        <Int Name="bsp_max_tree_depth" Label="BSP Maximum Tree Depth" AdvanceLevel="1">
          <Categories>
            <Cat>Genre</Cat>
          </Categories>
          <DefaultValue>10</DefaultValue>
          <Range>
            <Min inclusive="false">0</Min>
          </Range>
        </Int>
        <Int Name="bsp_min_leaf_length" Label="BSP Minimum Leaf Length" AdvanceLevel="1">
          <Categories>
            <Cat>Genre</Cat>
          </Categories>
          <DefaultValue>25</DefaultValue>
          <Range>
            <Min inclusive="false">0</Min>
          </Range>
        </Int>
        <Double Name="spatial_tolerance" Label="Spatial Tolerance" AdvanceLevel="1">
          <Categories>
            <Cat>Genre</Cat>
          </Categories>
          <DefaultValue>1.0e-8</DefaultValue>
          <Range>
            <Min inclusive="false">0.0</Min>
          </Range>
        </Double>
        <Double Name="smoothing_tolerance" Label="Smoothing Tolerance" AdvanceLevel="1">
          <Categories>
            <Cat>Genre</Cat>
          </Categories>
          <DefaultValue>1.0e-8</DefaultValue>
          <Range>
            <Min inclusive="false">0.0</Min>
          </Range>
        </Double>
        <Int Name="smoothing_max_iter" Label="Smoothing Maxiumum Iterations" AdvanceLevel="1">
          <Categories>
            <Cat>Genre</Cat>
          </Categories>
          <DefaultValue>100</DefaultValue>
          <Range>
            <Min inclusive="true">0</Min>
          </Range>
        </Int>
        <Double Name="smoothing_weight" Label="Smoothing Weight" AdvanceLevel="1">
          <Categories>
            <Cat>Genre</Cat>
          </Categories>
          <DefaultValue>2.0</DefaultValue>
          <Range>
            <Min inclusive="false">0.0</Min>
          </Range>
        </Double>
        <!-- Truchas-specific item defs-->
        <Double Name="ambient-temperature" Label="Ambient Temperature">
          <Categories>
            <Cat>Enclosure Radiation</Cat>
          </Categories>
          <ExpressionType>fn.er.ambient-temperature</ExpressionType>
        </Double>
        <Double Name="error_tolerance" Label="Error Tolerance">
          <Categories>
            <Cat>Enclosure Radiation</Cat>
          </Categories>
          <DefaultValue>0.001</DefaultValue>
        </Double>
        <Void Name="skip_geometry_check" Label="Skip Geometry Check" Optional="true" EnabledByDefault="false" AdvanceLevel="1">
          <Categories>
            <Cat>Enclosure Radiation</Cat>
          </Categories>
        </Void>
        <String Name="precon_method" Label="Preconditioner" AdvanceLevel="1">
          <Categories>
            <Cat>Enclosure Radiation</Cat>
          </Categories>
          <DiscreteInfo DefaultIndex="0">
            <Value>jacobi</Value>
            <Value>chebyshev</Value>
          </DiscreteInfo>
        </String>
        <Int Name="precon_iter" Label="Number of Preconditioner Iterations" AdvanceLevel="1">
          <Categories>
            <Cat>Enclosure Radiation</Cat>
          </Categories>
          <DefaultValue>1</DefaultValue>
          <RangeInfo>
            <Min inclusive="true">1</Min>
          </RangeInfo>
        </Int>
        <String Name="precon_coupling_method" Label="Preconditioner Coupling Method" AdvanceLevel="1">
          <Categories>
            <Cat>Enclosure Radiation</Cat>
          </Categories>
          <DiscreteInfo DefaultIndex="2">
            <Value>jacobi</Value>
            <Value>forward GS</Value>
            <Value>backward GS</Value>
            <Value>factorization</Value>
          </DiscreteInfo>
        </String>
      </ItemDefinitions>
    </AttDef>
    <AttDef Type="ignore-blocks" Label="Ignore Blocks" Unique="true" Version="0">
      <AssociationsDef NumberOfRequiredValues="0" Extensible="true">
        <Accepts>
          <Resource Name="smtk::model::Resource" Filter="volume"/>
        </Accepts>
      </AssociationsDef>
      <BriefDescription>Specify element blocks to be ignored by Genre</BriefDescription>
      <Categories>
        <Cat>Genre</Cat>
      </Categories>
    </AttDef>
    <AttDef Type="emissivity" Label="Surface Emissivity" Unique="true" Version="0">
      <AssociationsDef NumberOfRequiredValues="1" Extensible="true">
        <Accepts>
          <Resource Name="smtk::model::Resource" Filter="face"/>
        </Accepts>
      </AssociationsDef>
      <Categories>
        <Cat>Enclosure Radiation</Cat>
      </Categories>
      <ItemDefinitions>
        <Double Name="emissivity" Label="Emissivity" Version="0">
          <ExpressionType>fn.er.emissivity</ExpressionType>
          <RangeInfo>
            <Min Inclusive="true">0.0</Min>
            <Max Inclusive="true">1.0</Max>
          </RangeInfo>
        </Double>
      </ItemDefinitions>
    </AttDef>
  </Definitions>
</SMTK_AttributeResource>
