//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
// .NAME smtkTruchasMaterialPriorityView.h - SMTK view for flow material priority

#ifndef smtk_simulation_truchas_qt_smtkTruchasMaterialPriorityView_h
#define smtk_simulation_truchas_qt_smtkTruchasMaterialPriorityView_h

#include "smtk/PublicPointerDefs.h"
#include "smtk/SharedFromThis.h"
#include "smtk/extension/qt/qtBaseAttributeView.h"

class smtkTruchasMaterialPriorityViewInternals;

/* **
  Provides a widget encapsulating a custom editor for the Truchas
  material_priority item used in FLOW namelists. Takes as input a
  "material-priority" attribute.

  The first implementation expects that each material will include,
  at most, only one fluid phase.
 */

typedef smtk::extension::qtBaseView qtBaseView;
typedef smtk::extension::qtBaseAttributeView qtBaseAttributeView;

class smtkTruchasMaterialPriorityView : public qtBaseAttributeView
{
  Q_OBJECT

public:
  smtkTypenameMacro(smtkTruchasMaterialPriorityView);

  static qtBaseView* createViewWidget(const smtk::view::Information& info);
  smtkTruchasMaterialPriorityView(const smtk::view::Information& info);
  virtual ~smtkTruchasMaterialPriorityView();

  bool isEmpty() const override;
  bool isValid() const override;

protected slots:
  void onPriorityConfirmed();
  void onPriorityModified(smtk::attribute::ItemPtr, bool needsConfirm);
  void onTrackingStateChanged(int state);

protected:
  void createWidget() override;
  void setupConnections();
  void updateUI() override;

private:
  smtkTruchasMaterialPriorityViewInternals* m_internals;
};

#endif // smtk_simulation_truchas_qt_smtkTruchasMaterialPriorityView_h
