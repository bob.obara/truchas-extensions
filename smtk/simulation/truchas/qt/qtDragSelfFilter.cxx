//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================

#include "qtDragSelfFilter.h"

#include <QDebug>
#include <QDragMoveEvent>
#include <QEvent>

qtDragSelfFilter::qtDragSelfFilter(QObject* parent)
  : QObject(parent)
{
}

bool qtDragSelfFilter::eventFilter(QObject* obj, QEvent* event)
{
  // qDebug() << "Event" << event->type();
  if (event->type() == QEvent::DragEnter)
  {
    auto dragMoveEvent = dynamic_cast<QDragMoveEvent*>(event);
    // qDebug() << event->type() << ", " << dragMoveEvent->source() << ", obj: " << obj;
    if (dragMoveEvent->source() == this->parent())
    {
      dragMoveEvent->acceptProposedAction();
      return false;
    }
    return true;
  }

  return QObject::eventFilter(obj, event);
}
