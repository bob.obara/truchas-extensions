//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================

#ifndef smtk_simulation_truchas_operations_Read_h
#define smtk_simulation_truchas_operations_Read_h

#include "smtk/simulation/truchas/Exports.h"

#include "smtk/project/Operation.h"

#include <string>

namespace smtk
{
namespace simulation
{
namespace truchas
{
class SMTKTRUCHAS_EXPORT LegacyRead : public smtk::project::Operation
{
public:
  smtkTypeMacro(smtk::simulation::truchas::LegacyRead);
  smtkCreateMacro(LegacyRead);
  smtkSharedFromThisMacro(smtk::project::Operation);

protected:
  Result operateInternal() override;
  virtual const char* xmlDescription() const override;
};

SMTKCORE_EXPORT smtk::resource::ResourcePtr read(const std::string&);
}
}
}
#endif
