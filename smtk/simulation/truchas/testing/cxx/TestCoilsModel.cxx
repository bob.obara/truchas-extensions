//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================

#include "smtk/attribute/DoubleItem.h"
#include "smtk/attribute/GroupItem.h"
#include "smtk/attribute/IntItem.h"
#include "smtk/attribute/Resource.h"
#include "smtk/io/AttributeReader.h"
#include "smtk/io/Logger.h"

#include "smtk/simulation/truchas/qt/qtInductionCoilsModel.h"

#include "helpers.h"

#include <QDir>
#include <QString>
#include <Qt>

namespace
{
QDir dataRoot(DATA_DIR);
}

int TestCoilsModel(int, char**)
{
  // Load attribute file
  std::string attFile = dataRoot.absoluteFilePath("induction-heating.sbi").toStdString();
  auto attResource = smtk::attribute::Resource::create();
  auto attReader = smtk::io::AttributeReader();
  auto logger = smtk::io::Logger::instance();
  bool err = attReader.read(attResource, attFile, logger);
  smtkTest(!err, "Error reading attribute file" << attFile);

  // Use induction-heating attribute to initialize coils model
  std::string attName = "induction-heating";
  auto att = attResource->findAttribute(attName);
  smtkTest(att != nullptr, "Error finding " << attName << " attribute");

  auto coilsModel = new qtInductionCoilsModel(nullptr, att);
  test(coilsModel != nullptr, "Failed to create qtInductionCoilsModel");

  /*
    The attribute file contains 2 coils:
    #turns   radius   length     x     y     z
       4    3.14159   2.71828  0.01  0.02  0.03
       1     6.28    99.9      0.0   0.0   1.0
  */

  int nrows = coilsModel->rowCount();
  smtkTest(nrows == 2, "Wrong number of rows, should be 2 not " << nrows);

  // Coils table has 6 columns (#turns, radius, length, x, y, z)
  int ncols = coilsModel->columnCount();
  smtkTest(ncols == 6, "Wrong number of columns, should be 6 not " << ncols);

  // Check values
  QModelIndex index;
  QVariant variant;
  Qt::ItemFlags flags;
  bool comp;
  bool ok;
  auto coilsItem = att->findGroup("coils");

  // NTurns (column 0)
  index = coilsModel->index(0, 0);
  variant = coilsModel->data(index);
  test(variant.isValid(), "Variant not valid");
  test(variant.toInt() == 4, "NTurns data at index (0,0) incorrect, should be 4");
  // Change value
  int nturns = 5;
  ok = coilsModel->setData(index, nturns);
  test(ok, "Failed to set coil 1 NTurns");
  auto ntItem = coilsItem->findAs<smtk::attribute::IntItem>(0, "nturns");
  smtkTest(
    ntItem->value() == nturns, "NTurns setData at index (0,0) incorrect; should be " << nturns);

  // Radius (column 1)
  index = coilsModel->index(0, 1);
  variant = coilsModel->data(index);
  test(variant.isValid(), "Variant not valid");
  comp = compareDouble(variant.toDouble(), 3.14159);
  test(comp, "Radius at index (0,0) incorrect, should be 3.14159");
  // Change value
  double e = 2.71828;
  ok = coilsModel->setData(index, QVariant(e));
  test(ok, "Failed to set coil 1 radius value");
  auto radiusItem = coilsItem->findAs<smtk::attribute::DoubleItem>(0, "radius");
  comp = compareDouble(radiusItem->value(), e);
  smtkTest(comp, "Radius setData at index (0,1) incorrect; should be 2.71828, not "
      << radiusItem->value());

  // Length (column 2)
  index = coilsModel->index(0, 2);
  variant = coilsModel->data(index);
  test(variant.isValid(), "Variant not valid");
  comp = compareDouble(variant.toDouble(), 2.71828);
  smtkTest(comp, "Length at index (0,2) incorrect, should be 2.71828, not " << variant.toDouble());
  // Also check that item is enabled
  flags = coilsModel->flags(index);
  test(flags & Qt::ItemIsEnabled, "Coil 1 length entry should NOT be disabled");

  // Change value
  double pi2 = 6.28;
  ok = coilsModel->setData(index, QVariant(pi2));
  test(ok, "Failed to set coil 1 radius value");
  auto lengthItem = coilsItem->findAs<smtk::attribute::DoubleItem>(0, "length");
  comp = compareDouble(lengthItem->value(), pi2);
  smtkTest(comp, "Length setData at index (0,2) incorrect; should be " << pi2 << ", not "
                                                                       << lengthItem->value());

  // Length for coil 2 should be disabled (because number of turns is 1)
  index = coilsModel->index(1, 2);
  flags = coilsModel->flags(index);
  test((flags & Qt::ItemIsEnabled) == 0x0, "Coil 2 length entry should be disabled");

  return 0;
}
