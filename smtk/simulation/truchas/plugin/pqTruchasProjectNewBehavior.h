//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#ifndef smtk_simulation_truchas_plugin_pqTruchasProjectNewBehavior_h
#define smtk_simulation_truchas_plugin_pqTruchasProjectNewBehavior_h

#include "smtk/PublicPointerDefs.h"

#include "pqReaction.h"

#include <QObject>

/// A reaction for writing a resource manager's state to disk.
class pqTruchasProjectNewReaction : public pqReaction
{
  Q_OBJECT
  typedef pqReaction Superclass;

public:
  /// Constructor. Parent cannot be NULL.
  pqTruchasProjectNewReaction(QAction* parent);
  ~pqTruchasProjectNewReaction() = default;

protected:
  /// Called when the action is triggered.
  void onTriggered() override;

private:
  Q_DISABLE_COPY(pqTruchasProjectNewReaction)
};

/// \brief Add a menu item for writing the state of the resource manager.
class pqTruchasProjectNewBehavior : public QObject
{
  Q_OBJECT
  using Superclass = QObject;

public:
  static pqTruchasProjectNewBehavior* instance(QObject* parent = nullptr);
  ~pqTruchasProjectNewBehavior() override;

  void newProject();

signals:
  void projectCreated(smtk::project::ProjectPtr);

protected:
  pqTruchasProjectNewBehavior(QObject* parent = nullptr);

private:
  class Internal;
  Internal* m_internal;

  Q_DISABLE_COPY(pqTruchasProjectNewBehavior);
};

#endif
