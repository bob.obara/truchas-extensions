//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#include "smtk/simulation/truchas/plugin/pqTruchasDisplayToolBar.h"

// SMTK
#include "smtk/extension/paraview/appcomponents/pqSMTKResourceRepresentation.h"
#include "smtk/extension/paraview/server/vtkSMTKResourceRepresentation.h"
#include "smtk/model/Entity.h"
#include "smtk/model/Resource.h"
#include "smtk/resource/Component.h"
#include "smtk/resource/Properties.h"
#include "smtk/resource/Resource.h"

// ParaView
#include "pqActiveObjects.h"
#include "pqRepresentation.h"
#include "pqView.h"
#include "vtkCompositeRepresentation.h"
#include "vtkSMProxy.h"

// Qt
#include <QAction>
#include <QDebug>
#include <QIcon>
#include <QList>
#include <QWidget>

pqTruchasDisplayToolBar::pqTruchasDisplayToolBar(QWidget* parent)
  : Superclass("SMTK Display Toolbar", parent)
{
  this->setObjectName("SMTK Display Toolbar");

  this->addAction(QIcon(":/icons/toolbar/eye_volume.svg"), "Only show volume entities", this,
    &pqTruchasDisplayToolBar::showVolumes);
  this->addAction(QIcon(":/icons/toolbar/eye_surface.svg"), "Only show surface entities", this,
    &pqTruchasDisplayToolBar::showSurfaces);
  this->addAction(QIcon(":/icons/toolbar/eye_coil.svg"), "Toggle EM Mesh Visibility", this,
    &pqTruchasDisplayToolBar::toggleEMMeshVisibility);
}

pqTruchasDisplayToolBar::~pqTruchasDisplayToolBar() = default;

void pqTruchasDisplayToolBar::showSurfaces()
{
  this->showEntities(smtk::model::FACE);
}

void pqTruchasDisplayToolBar::showVolumes()
{
  this->showEntities(smtk::model::VOLUME);
}

void pqTruchasDisplayToolBar::toggleEMMeshVisibility()
{
  // Find EM mesh from active view (is there a better way to do this?)
  pqView* activeView = pqActiveObjects::instance().activeView();
  if (activeView == nullptr)
  {
    return;
  }

  QList<pqRepresentation*> repList = activeView->getRepresentations();
  for (auto rep : repList)
  {
    auto resource = this->getResource(rep);
    if (resource->isOfType<smtk::model::Resource>())
    {
      if (resource->properties().contains<std::string>("project.use"))
      {
        std::string prop = resource->properties().get<std::string>()["project.use"];
        if (prop == "induction-heating-mesh")
        {
          bool isVis = rep->isVisible();
          rep->setVisible(!isVis);
          activeView->render();
          break; // only 1 emag model
        }
      }
    }
  }
}

smtk::resource::ResourcePtr pqTruchasDisplayToolBar::getResource(pqRepresentation* rep) const
{
  auto resourceRep = dynamic_cast<pqSMTKResourceRepresentation*>(rep);
  if (resourceRep == nullptr)
  {
    return nullptr;
  }

  auto proxy = resourceRep->getProxy();
  auto clientRep = proxy->GetClientSideObject(); // TODO: Remove the need for me.
  auto compositeRep = vtkCompositeRepresentation::SafeDownCast(clientRep);
  if (compositeRep == nullptr)
  {
    return nullptr;
  }
  auto serverRep =
    vtkSMTKResourceRepresentation::SafeDownCast(compositeRep->GetActiveRepresentation());
  if (serverRep == nullptr)
  {
    return nullptr;
  }

  smtk::resource::ResourcePtr resource = serverRep->GetResource();
  return resource;
}

void pqTruchasDisplayToolBar::showEntities(smtk::model::BitFlags flags) const
{
  // Get active view
  pqView* activeView = pqActiveObjects::instance().activeView();
  if (activeView == nullptr)
  {
    return;
  }

  // Get representations from active view
  QList<pqRepresentation*> repList = activeView->getRepresentations();
  for (auto rep : repList)
  {
    auto resource = this->getResource(rep);
    auto modelResource = std::dynamic_pointer_cast<smtk::model::Resource>(resource);
    if (modelResource == nullptr)
    {
      continue;
    }

    // Only update the heat transfer mesh. For present use cases, the
    // induction heating mesh only has (volume) element blocks.
    if (!resource->properties().contains<std::string>("project.use"))
    {
      continue;
    }

    std::string prop = resource->properties().get<std::string>()["project.use"];
    if (prop != "heat-transfer-mesh")
    {
      continue;
    }

    // Visit all model components and set visibility to match flags
    auto resourceRep = dynamic_cast<pqSMTKResourceRepresentation*>(rep);
    smtk::resource::Component::Visitor componentVisitor = [this, resourceRep, flags](
      const smtk::resource::ComponentPtr& component) {
      this->displayVisitor(component, resourceRep, flags);
    };

    modelResource->visit(componentVisitor);
  }
  activeView->render();
}

void pqTruchasDisplayToolBar::displayVisitor(const smtk::resource::ComponentPtr& component,
  pqSMTKResourceRepresentation* rep, smtk::model::BitFlags flags) const
{
  auto modelEnt = component->as<smtk::model::Entity>();
  if ((modelEnt != nullptr) && (modelEnt->dimension() >= 0))
  {
    bool visible = modelEnt->entityFlags() == flags;
    rep->setVisibility(component, visible);
  }
}
